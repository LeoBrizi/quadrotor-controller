function script()
    % Initialization 
    clc;
    close all;
    clear script;

    PORT = 19997;
    attitudeControl = false;
    p_des = zeros(1, 3);
    v_des = zeros(1, 3);
    a_des = zeros(1, 3);

    roll_des_old = 0;
    pitch_des_old = 0;
    yaw_des_old = 0;
    e_int_x = 0;
    e_int_y = 0;
    e_int_z = 0;
    k_i = 0;

    mass_uav = 0.5;
    g = 9.81;
    Ts = 0.05;
    initialize = false;

    % Gains %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    k_z_p = 3;
    k_z_v = 7.275;
    k_z_i = 0.001;

    k_x_p = 1.25;
    k_x_v = 1.5;
    k_x_i = 0.001;

    k_y_p = 1.25;
    k_y_v = 1.5;
    k_y_i = 0.001;

    kroll_p = 0.0627;
    kroll_d = 0.0783;

    kpitch_p = 0.0313;
    kpitch_d = 0.0470;

    kyaw_p = 0.0334;
    kyaw_d = 0.0334;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    sim = remApi('remoteApi');
    sim.simxFinish(-1);

    clientID = sim.simxStart('127.0.0.1', PORT, true, true, 5000, 5);

    cleanupObj = onCleanup(@()cleanMeUp(sim, clientID));
    
    if (clientID > -1)

        disp('Connected to simulator');

        % Enable the synchronous mode on the client
        sim.simxSynchronous(clientID, true); 

        % Start simulation
        sim.simxStartSimulation(clientID, sim.simx_opmode_oneshot);

        % Set controller flag
        if ~attitudeControl
            sim.simxSetIntegerSignal(clientID, 'use_embedded_attitude_control', true, sim.simx_opmode_oneshot);
        else
            sim.simxSetIntegerSignal(clientID, 'use_embedded_attitude_control', false, sim.simx_opmode_oneshot);
        end

        % Get handle uav
        [~, uav] = sim.simxGetObjectHandle(clientID, 'AscTed_Hummingbird_dynamic', sim.simx_opmode_blocking);

        % Eneable streaming %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Time (debug purpose)
        sim.simxGetFloatSignal(clientID, 'simulationTime', sim.simx_opmode_streaming);
        % Desired values 
        sim.simxGetFloatSignal(clientID, 'Reference/position/x', sim.simx_opmode_streaming);
        sim.simxGetFloatSignal(clientID, 'Reference/position/y', sim.simx_opmode_streaming);
        sim.simxGetFloatSignal(clientID, 'Reference/position/z', sim.simx_opmode_streaming);
        sim.simxGetFloatSignal(clientID, 'Reference/velocity/x', sim.simx_opmode_streaming);
        sim.simxGetFloatSignal(clientID, 'Reference/velocity/y', sim.simx_opmode_streaming);
        sim.simxGetFloatSignal(clientID, 'Reference/velocity/z', sim.simx_opmode_streaming);
        sim.simxGetFloatSignal(clientID, 'Reference/acceleration/x', sim.simx_opmode_streaming);
        sim.simxGetFloatSignal(clientID, 'Reference/acceleration/y', sim.simx_opmode_streaming);
        sim.simxGetFloatSignal(clientID, 'Reference/acceleration/z', sim.simx_opmode_streaming); 
        % Sensored values
        sim.simxGetObjectPosition(clientID, uav, -1, sim.simx_opmode_streaming); 
        sim.simxGetObjectVelocity(clientID, uav, sim.simx_opmode_streaming);  
        sim.simxGetObjectOrientation(clientID, uav, -1, sim.simx_opmode_streaming); 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Step
        sim.simxSynchronousTrigger(clientID);
        sim.simxGetPingTime(clientID);  

        % Update exit condition
        [~, server_state] = sim.simxGetInMessageInfo(clientID, sim.simx_headeroffset_server_state);
        is_running = bitand(server_state, 1);

        while is_running

            % Get signals %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Time (debug purpose)
            [~, t] = sim.simxGetFloatSignal(clientID, 'simulationTime', sim.simx_opmode_buffer);
            % Desired values 
            [~, p_des(1)] = sim.simxGetFloatSignal(clientID, 'Reference/position/x', sim.simx_opmode_buffer);
            [~, p_des(2)] = sim.simxGetFloatSignal(clientID, 'Reference/position/y', sim.simx_opmode_buffer);
            [~, p_des(3)] = sim.simxGetFloatSignal(clientID, 'Reference/position/z', sim.simx_opmode_buffer);
            [~, v_des(1)] = sim.simxGetFloatSignal(clientID, 'Reference/velocity/x', sim.simx_opmode_buffer);
            [~, v_des(2)] = sim.simxGetFloatSignal(clientID, 'Reference/velocity/y', sim.simx_opmode_buffer);
            [~, v_des(3)] = sim.simxGetFloatSignal(clientID, 'Reference/velocity/z', sim.simx_opmode_buffer);
            [~, a_des(1)] = sim.simxGetFloatSignal(clientID, 'Reference/acceleration/x', sim.simx_opmode_buffer);
            [~, a_des(2)] = sim.simxGetFloatSignal(clientID, 'Reference/acceleration/y', sim.simx_opmode_buffer);
            [~, a_des(3)] = sim.simxGetFloatSignal(clientID, 'Reference/acceleration/z', sim.simx_opmode_buffer); 
            % Sensored values
            [~, p] = sim.simxGetObjectPosition(clientID, uav, -1, sim.simx_opmode_buffer); 
            [~, v, w] = sim.simxGetObjectVelocity(clientID, uav, sim.simx_opmode_buffer);  
            [~, abg] = sim.simxGetObjectOrientation(clientID, uav, -1, sim.simx_opmode_buffer); 
            r = eul2rotm(abg, 'XYZ');

            pitch = atan2(-r(3,1), sqrt(r(3,2)^2 + r(3,3)^2));
            roll = atan2(r(3,2)/cos(pitch), r(3,3)/cos(pitch));
            yaw = atan2(r(2,1)/cos(pitch), r(1,1)/cos(pitch));

            % Compute desired values  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
            % Errors        
            e_x_p = p_des(1) - p(1);
            e_y_p = p_des(2) - p(2);
            e_z_p = p_des(3) - p(3);

            e_int_x = tanh(e_int_x + e_x_p);
            e_int_y = tanh(e_int_y + e_y_p);
            e_int_z = tanh(e_int_z + e_z_p);

            e_x_v = v_des(1) - v(1);
            e_y_v = v_des(2) - v(2);
            e_z_v = v_des(3) - v(3);
            
            if ~initialize
                p_des_in = p_des;
                initialize = true;
            elseif (t > 0 && t < 0.051) && (isequal(p_des_in, p_des))
                k_z_p = 1;
                k_z_v = 2;

                k_x_p = 1;
                k_x_v = 2;

                k_y_p = 1;
                k_y_v = 2;
            end
            
            if t > 10
                k_i = 1;
            end
            % Thrust
            T = ( mass_uav/(cos(pitch)*cos(roll)) ) * ( +g + a_des(3) + k_z_p * e_z_p + k_z_v * e_z_v + k_i * k_z_i * e_int_z );
            
            Tx = ( mass_uav/T ) * ( k_x_p * e_x_p + k_x_v * e_x_v + a_des(1) + k_i * k_x_i * e_int_x);
            Ty = ( mass_uav/T ) * ( k_y_p * e_y_p + k_y_v * e_y_v + a_des(2) + k_i * k_y_i * e_int_y);

            % Angles 
            roll_des = wrapToPi( asin( Tx*sin(yaw) - Ty*cos(yaw)) ); 
            pitch_des = wrapToPi( asin( (Tx*cos(yaw) + Ty*sin(yaw))/cos(roll_des) ) );

            yaw_des = 0; 
            
            if attitudeControl
                % Torque
                err_roll = roll_des - roll;
                err_pitch = pitch_des - pitch;
                err_yaw = yaw_des - yaw;
                
                v_roll_des = (roll_des-roll_des_old)/Ts;
                v_pitch_des = (pitch_des-pitch_des_old)/Ts;
                v_yaw_des = (yaw_des-yaw_des_old)/Ts;
                roll_des_old = roll_des;
                pitch_des_old = pitch_des;
                yaw_des_old = yaw_des;

                err_v_roll = v_roll_des - w(1);
                err_v_pitch = v_pitch_des - w(2);
                err_v_yaw = v_yaw_des - w(3);

                tau_phi = kroll_p * err_roll + kroll_d * err_v_roll;
                tau_theta = kpitch_p * err_pitch + kpitch_d * err_v_pitch;
                tau_psi = kyaw_p * err_yaw + kyaw_d * err_v_yaw;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Send signals (T + reference orientation angles)
            sim.simxSetFloatSignal(clientID, 'ControlInputs/Thrust', T, sim.simx_opmode_oneshot);
            sim.simxSetFloatSignal(clientID, 'ControlInputs/roll_des', roll_des, sim.simx_opmode_oneshot);
            sim.simxSetFloatSignal(clientID, 'ControlInputs/pitch_des', pitch_des, sim.simx_opmode_oneshot);
            sim.simxSetFloatSignal(clientID, 'ControlInputs/yaw_des', yaw_des, sim.simx_opmode_oneshot);
            if attitudeControl
                sim.simxSetFloatSignal(clientID, 'ControlInputs/tau_roll', tau_phi, sim.simx_opmode_oneshot);
                sim.simxSetFloatSignal(clientID, 'ControlInputs/tau_pitch', tau_theta, sim.simx_opmode_oneshot);
                sim.simxSetFloatSignal(clientID, 'ControlInputs/tau_yaw', tau_psi, sim.simx_opmode_oneshot);
            end

            % Step
            sim.simxSynchronousTrigger(clientID);
            sim.simxGetPingTime(clientID);  

            % Update exit condition
            [~, server_state] = sim.simxGetInMessageInfo(clientID, sim.simx_headeroffset_server_state);
            is_running = bitand(server_state, 1);
            
        end

        % Disable streaming %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Time (debug purpose)
        sim.simxGetFloatSignal(clientID, 'simulationTime', sim.simx_opmode_discontinue);
        % Desired values 
        sim.simxGetFloatSignal(clientID, 'Reference/position/x', sim.simx_opmode_discontinue);
        sim.simxGetFloatSignal(clientID, 'Reference/position/y', sim.simx_opmode_discontinue);
        sim.simxGetFloatSignal(clientID, 'Reference/position/z', sim.simx_opmode_discontinue);
        sim.simxGetFloatSignal(clientID, 'Reference/velocity/x', sim.simx_opmode_discontinue);
        sim.simxGetFloatSignal(clientID, 'Reference/velocity/y', sim.simx_opmode_discontinue);
        sim.simxGetFloatSignal(clientID, 'Reference/velocity/z', sim.simx_opmode_discontinue);
        sim.simxGetFloatSignal(clientID, 'Reference/acceleration/x', sim.simx_opmode_discontinue);
        sim.simxGetFloatSignal(clientID, 'Reference/acceleration/y', sim.simx_opmode_discontinue);
        sim.simxGetFloatSignal(clientID, 'Reference/acceleration/z', sim.simx_opmode_discontinue); 
        % Sensored values
        sim.simxGetObjectPosition(clientID, uav, -1, sim.simx_opmode_discontinue); 
        sim.simxGetObjectVelocity(clientID, uav, sim.simx_opmode_discontinue);  
        sim.simxGetObjectOrientation(clientID, uav, -1, sim.simx_opmode_discontinue); 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Closing connection
        sim.simxFinish(clientID);
        sim.delete();

    else
        disp('Error in connection');
    end
    
    % Fires when main function terminates
    function cleanMeUp(sim, clientID)
        sim.simxStopSimulation(clientID, sim.simx_opmode_oneshot);  
    end
end
